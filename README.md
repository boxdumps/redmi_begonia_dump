## begonia-user 11 RP1A.200720.011 V12.5.5.0.RGGMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6785
- Codename: begonia
- Brand: Redmi
- Flavor: lineage_begonia-userdebug
- Release Version: 12
- Id: SQ3A.220705.003.A1
- Incremental: 1657984588
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/begonia/begonia:11/RP1A.200720.011/V12.5.5.0.RGGMIXM:user/release-keys
- OTA version: 
- Branch: begonia-user-11-RP1A.200720.011-V12.5.5.0.RGGMIXM-release-keys-random-text-28640245401105
- Repo: redmi_begonia_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
